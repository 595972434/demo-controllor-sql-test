package com.twu.demo.controllor;

import com.twu.demo.entity.ProductLineRepository;
import com.twu.demo.entity.ProductRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ProductControllor {

    private ProductRepository productRepository;
    private ProductLineRepository productLineRepository;

    public ProductControllor(ProductRepository productRepository, ProductLineRepository productLineRepository) {
        this.productRepository = productRepository;
        this.productLineRepository = productLineRepository;
    }

    @GetMapping("/produc")
    public String getAllProduct(){
        System.out.println(productLineRepository.findByProductLine("Trains").getTextDescription());
        return "nihao";
    }
    @GetMapping("/product")
    public ResponseEntity getAllProducts(){
        return ResponseEntity.status(201)
                .body(productRepository.findAll());
    }

    @GetMapping("/productLine")
    public ResponseEntity grtAllProductLines(){
        System.out.println(productLineRepository.findByProductLine("Trains").getTextDescription());
        return ResponseEntity.status(201)
                .body(productLineRepository.findAll());
    }
}
